import {SCJobPosting, SCThingOriginType, SCThingType} from '@openstapps/core';

export const expected13736: SCJobPosting = {
  identifiers: {
    job_id: '13736',
  },
  name: 'Test Job Without Organization',
  employerOverview: {
    type: SCThingType.Organization,
    name: 'Test Org without org details',
    uid: 'a814fd00-890a-5033-996e-4f02dc85cb4a',
    identifiers: {
      org_id: '53',
    },
    image: 'https://api.stellenportal-uni-frankfurt.de/storage/org/53/TestOrgWithoutDetails.png',
  },
  image: 'https://api.stellenportal-uni-frankfurt.de/storage/org/53/TestOrgWithoutDetails.png',
  inPlace: {
    type: SCThingType.PointOfInterest,
    uid: '1a0a8607-c066-5764-98f9-30d1d519e6d3',
    name: 'Location of job with id 13736',
    geo: {
      point: {
        type: 'Point',
        coordinates: [0, 0],
      },
    },
    categories: [],
    address: {
      addressCountry: 'de',
      addressLocality: 'Frankfurt am Main',
      postalCode: '60323',
      streetAddress: '',
    },
  },
  sameAs: 'https://stellenportal-uni-frankfurt.de/jobdetails/?job=13736',
  description: 'Description for test job without organization',
  type: SCThingType.JobPosting,
  uid: '1a0a8607-c066-5764-98f9-30d1d519e6d3',
  origin: {
    indexed: '2023-10-15T15:40:36.672Z',
    name: 'Goethe-Uni Stellenportal',
    type: SCThingOriginType.Remote,
    url: 'https://api.stellenportal-uni-frankfurt.de/job?id=13736',
  },
  categories: ['business', 'other', 'arts', 'social', 'communication'],
};

export const expected14125: SCJobPosting = {
  identifiers: {
    job_id: '14125',
  },
  name: 'Test Job With Organization',
  employerOverview: {
    type: SCThingType.Organization,
    name: 'Test Organization',
    uid: 'a28e9f3f-481f-51c2-8102-05f06cfea986',
    identifiers: {
      org_id: '30',
    },
    image: 'https://api.stellenportal-uni-frankfurt.de/storage/org/30/TestOrg.png',
    description: 'Test description',
    sameAs: 'https://stellenportal-uni-frankfurt.de/firmentreffer/?id=30',
  },
  image: 'https://api.stellenportal-uni-frankfurt.de/storage/org/30/TestOrg.png',
  inPlace: {
    type: SCThingType.PointOfInterest,
    uid: '8b648e55-2e5a-5fe9-a376-8f1c901ef32d',
    name: 'Location of job with id 14125',
    geo: {
      point: {
        type: 'Point',
        coordinates: [0, 0],
      },
    },
    categories: [],
    address: {
      addressCountry: 'de',
      addressLocality: 'Frankfurt am Main',
      postalCode: '60329',
      streetAddress: '',
    },
  },
  sameAs: 'https://stellenportal-uni-frankfurt.de/jobdetails/?job=14125',
  description: 'Test description for job with organization',
  type: SCThingType.JobPosting,
  uid: '8b648e55-2e5a-5fe9-a376-8f1c901ef32d',
  origin: {
    indexed: '2023-10-15T15:40:36.672Z',
    name: 'Goethe-Uni Stellenportal',
    type: SCThingOriginType.Remote,
    url: 'https://api.stellenportal-uni-frankfurt.de/job?id=14125',
  },
  categories: ['business'],
};
