import {MockAgent, setGlobalDispatcher} from 'undici';

export const mockAgent = new MockAgent();
setGlobalDispatcher(mockAgent);
mockAgent.disableNetConnect();
