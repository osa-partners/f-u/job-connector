/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingRemoteOrigin, SCThings, SCThingType} from '@openstapps/core';
import {Validator} from '@openstapps/core-tools';
import {expect} from 'chai';
import path from 'path';
import * as JobConnector from '../src/job-connector.js';
import * as url from 'url';
import {readFile} from 'fs/promises';
import {expected13736, expected14125} from './data/expected-data.js';
import {mockAgent} from './testbed.js';

/**
 * Gets the `SCThingType`-key as string
 *
 * @param instance Contains `type` with the value of a SCThingType-key
 */
function getSchemaNameFromType<T extends SCThings>(instance: T): string {
  const type = instance.type;
  const index = Object.values(SCThingType).indexOf(type);
  const key = Object.keys(SCThingType)[index];
  return `SC${key}`;
}

function removeIndexedTime<T extends SCThing>(thing: T): T {
  (thing.origin as SCThingRemoteOrigin).indexed = 'dont-compare';
  return thing;
}

describe('job-connector', function () {
  let validator: Validator;
  this.timeout(10_000);
  this.slow(5000);

  before(async function () {
    validator = new Validator();
    await validator.addSchemas(
      path.join(
        path.dirname(url.fileURLToPath(import.meta.url)),
        '..',
        'node_modules',
        '@openstapps',
        'core',
        'lib',
        'schema',
      ),
    );
    const jobs = await readFile('./test/data/jobs.json');
    const orgs = await readFile('./test/data/orgs.json');
    const job_13736 = await readFile('./test/data/job_13736.json');
    const job_14125 = await readFile('./test/data/job_14125.json');
    const org_30 = await readFile('./test/data/org_30.json');

    mockAgent
      .get('https://api.stellenportal-uni-frankfurt.de')
      .intercept({method: 'GET', path: '/jobs'})
      .reply(200, jobs)
      .times(1);

    mockAgent
      .get('https://api.stellenportal-uni-frankfurt.de')
      .intercept({method: 'GET', path: '/orgs'})
      .reply(200, orgs)
      .times(1);

    mockAgent
      .get('https://api.stellenportal-uni-frankfurt.de')
      .intercept({method: 'GET', path: '/job?id=13736'})
      .reply(200, job_13736)
      .times(1);

    mockAgent
      .get('https://api.stellenportal-uni-frankfurt.de')
      .intercept({method: 'GET', path: '/job?id=14125'})
      .reply(200, job_14125)
      .times(1);

    mockAgent
      .get('https://api.stellenportal-uni-frankfurt.de')
      .intercept({method: 'GET', path: '/org?id=14'})
      .reply(200, org_30)
      .times(1);
  });

  it('should get correct jobs', async function () {
    const jobs = await JobConnector.fetchJobs('f-u', 'Goethe-Uni Stellenportal');
    // validate results
    for (const job of jobs) {
      const schemaName = getSchemaNameFromType(job);
      const validatorResult = validator.validate(job, schemaName);
      expect(validatorResult.errors).to.have.lengthOf(
        0,
        JSON.stringify({
          errors: validatorResult.errors,
          job,
        }),
      );
    }
    // check against expected data
    expect(removeIndexedTime(jobs[0])).to.deep.equal(removeIndexedTime(expected14125));
    expect(removeIndexedTime(jobs[1])).to.deep.equal(removeIndexedTime(expected13736));
  });
});
