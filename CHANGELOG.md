## [1.0.2](https://gitlab.com/osa-partners/f-u/job-connector/compare/v1.0.1...v1.0.2) (2024-03-26)

### Bug Fixes

- filter unexpected large job offers ([8ac1005](https://gitlab.com/osa-partners/f-u/job-connector/commit/8ac1005be8b1d1daafe94fde8b9148749958c726))

## [1.0.1](https://gitlab.com/osa-partners/f-u/job-connector/compare/v1.0.0...v1.0.1) (2023-12-02)

### Bug Fixes

- Docker image creation ([cc858ca](https://gitlab.com/osa-partners/f-u/job-connector/commit/cc858cac5289250086bf83bc9d60d2aa613963b1))

# [1.0.0](https://gitlab.com/osa-partners/f-u/job-connector/compare/0b761afc80a8c6b24cec04de9ef4e6b4cb0160bb...v1.0.0) (2023-12-02)

### Features

- Initial Commit ([0b761af](https://gitlab.com/osa-partners/f-u/job-connector/commit/0b761afc80a8c6b24cec04de9ef4e6b4cb0160bb))
