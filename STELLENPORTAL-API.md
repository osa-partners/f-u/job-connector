# Stellenportal → GU-App

Schnittstellenbeschreibung v0.02

## Liste aktueller Firmenportraits:

[PATH] https://api.stellenportal-uni-frankfurt.de/orgs

[METHOD] GET

| KEY           | VALUE   | DESCRIPTION                                                     | DEFAULT | REQUIRED |
| ------------- | ------- | --------------------------------------------------------------- | ------- | -------- |
| amount        | INT     | Gibt an wieviele Jobs bei einem Request geliefert werden.       | 25      | -        |
| page          | INT     | Die Seite die angezeigt werden soll                             | -       | -        |
| search        | STRING  |                                                                 | -       | -        |
| law           | BOOLEAN |                                                                 | -       | -        |
| business      | BOOLEAN |                                                                 | -       | -        |
| natural       | BOOLEAN |                                                                 | -       | -        |
| education     | BOOLEAN |                                                                 | -       | -        |
| arts          | BOOLEAN |                                                                 | -       | -        |
| medical       | BOOLEAN |                                                                 | -       | -        |
| communication | BOOLEAN |                                                                 | -       | -        |
| law           | BOOLEAN |                                                                 | -       | -        |
| other         | BOOLEAN |                                                                 | -       | -        |
| type          | STRING  |                                                                 | -       | -        |
| zip_code      | INT     | Muß eine 5-stellige PLZ sein                                    | -       | -        |
| radius        | INT     | Hier wird in diesem Radius (abhängig von PLZ) nach Jobs gesucht | -       | -        |

---

## Details eines Unternehmens:

[PATH] https://api.stellenportal-uni-frankfurt.de/org

[METHOD] GET

| KEY | VALUE | DESCRIPTION                          | DEFAULT | REQUIRED |
| --- | ----- | ------------------------------------ | ------- | -------- |
| id  | INT   | Die portrait_id aus dem orgs-Request | -       | ja       |

```json
{
  "org": {
    "org_id": 521,
    "portrait_id": 485,
    "name": "Mainova AG ",
    "description": "<p>Mainova ist der f&uuml;hrende Energiedienstleister in Frankfurt am Main f&uuml;r Privat- und Firmenkund:innen in ganz Deutschland. Wie beliefern mehr als eine Million Menschen mit Strom, Gas, W&auml;rme und Wasser und &uuml;berzeugen als leistungsstarker Partner bei komplexen Gro&szlig;projekten und beim zukunftsgerichteten Ausbau von St&auml;dten. Wir entwickeln smarte Energiel&ouml;sungen und treiben aus &Uuml;berzeugung die Stadtentwicklung von morgen voran. Wir wollen unsere Heimatstadt noch lebenswerter machen.<br /><br />Ansprechpartnerin:</p><p>Inga Feuerherdt<br />Personalmarketing<br />Tel: +49 69 213 24724<br />E-Mail:<a href=\"mailto: i.feuerherdt@mainova.de\"> i.feuerherdt@mainova.de</a></p><p>&nbsp;</p>",
    "url": null,
    "url_broschure": null,
    "law": 1,
    "business": 1,
    "natural": 1,
    "it": 1,
    "education": 0,
    "other": 0,
    "arts": 0,
    "social": 0,
    "engineering": 1,
    "communication": 0,
    "medical": 0,
    "org_logo": "Mainova_Logo_freigestellt.png",
    "logo_path": "storage/org/521/Mainova_Logo_freigestellt.png",
    "departments": [
      {
        "id": 521,
        "head_office": 0,
        "zip_code": "0000",
        "city": "Guatemala",
        "street": "",
        "country": "other",
        "country_spec": "",
        "phone": "",
        "mail": ""
      }
    ],
    "links": [
      {
        "link": "https://www.mainova.de",
        "link_text": "Unternehmensseite"
      },
      {
        "link": "https://mainova.de/karriere",
        "link_text": "Karriereseite"
      },
      {
        "link": "https://linkedin.com/company/mainovaag",
        "link_text": null
      },
      {
        "link": "https://xing.com/companies/mainovaag",
        "link_text": null
      },
      {
        "link": "https://instagram.com/mainovaag/",
        "link_text": null
      },
      {
        "link": "https://facebook.com/MainovaAG",
        "link_text": null
      }
    ],
    "files": [
      {
        "file": "2022.12._MAI_A4_Broschuere_Professionals_final_WEB.pdf",
        "file_description": "Arbeiten bei Mainova - Broschüre",
        "file_link": null
      }
    ],
    "videos": [
      {
        "video_text": null,
        "video_link": "ZgpNKEd6ys8"
      },
      {
        "video_text": null,
        "video_link": "B05-h9W74O4"
      }
    ]
  }
}
```

---

## Liste aktueller Jobs:

[PATH] https://api.stellenportal-uni-frankfurt.de/jobs

[METHOD] GET

| KEY           | VALUE   | DESCRIPTION                                                     | DEFAULT | REQUIRED |
| ------------- | ------- | --------------------------------------------------------------- | ------- | -------- |
| amount        | INT     | Gibt an wieviele Jobs bei einem Request geliefert werden.       | 25      | -        |
| page          | INT     | Die Seite die angezeigt werden soll                             | -       | -        |
| search        | STRING  |                                                                 | -       | -        |
| law           | BOOLEAN |                                                                 | -       | -        |
| business      | BOOLEAN |                                                                 | -       | -        |
| natural       | BOOLEAN |                                                                 | -       | -        |
| education     | BOOLEAN |                                                                 | -       | -        |
| arts          | BOOLEAN |                                                                 | -       | -        |
| medical       | BOOLEAN |                                                                 | -       | -        |
| communication | BOOLEAN |                                                                 | -       | -        |
| law           | BOOLEAN |                                                                 | -       | -        |
| other         | BOOLEAN |                                                                 | -       | -        |
| type          | STRING  |                                                                 | -       | -        |
| zip_code      | INT     | Muß eine 5-stellige PLZ sein                                    | -       | -        |
| radius        | INT     | Hier wird in diesem Radius (abhängig von PLZ) nach Jobs gesucht | -       | -        |

## Details eines Jobs:

[PATH] https://api.stellenportal-uni-frankfurt.de/job

[METHOD] GET

| KEY | VALUE | DESCRIPTION                     | DEFAULT | REQUIRED |
| --- | ----- | ------------------------------- | ------- | -------- |
| id  | INT   | Die job_id aus dem jobs-Request | -       | ja       |

```json
{
  "job": {
    "job_id": 13883,
    "job_name": "2024 SUMMER INTERNSHIP PROGRAM [EMEA]",
    "description": "<p><strong>2024 SUMMER INTERNSHIP PROGRAM [EMEA]</strong><br />&nbsp;</p><p><strong>ABOUT BLACKROCK</strong></p><p>BlackRock&rsquo;s purpose is to help more and more people experience financial well-being. As a global investment manager and a leading provider of financial technology, our clients &ndash; from grandparents, doctors, and teachers to large institutions &ndash; turn to us for the solutions they need when planning for their most important goals. We are building a culture of innovation, curiosity, and compassion, one that enables every employee to make an impact.</p><p><strong>THE SUMMER INTERNSHIP PROGRAM</strong></p><p>Our Summer Internship Program is an 8-week, paid internship designed to provide you with an exciting, supportive and fun experience which mirrors life as an Analyst at BlackRock. You&rsquo;ll have real-world responsibilities during the summer, in addition to participating in professional development, networking and social events.</p><p>The Summer Internship Program takes place in June &ndash; August 2024 and acts as a feeder to the Full-Time Analyst Program for the following year.</p><p>To succeed at BlackRock, you should be curious and motivated to solve some of the world&rsquo;s most complex challenges. You should be willing to step outside your comfort zone, open to new ways of thinking and passionate about helping people from all walks of life build a better financial future.</p><p><strong><a href=\"https://careers.blackrock.com/early-careers/europe/\">Learn more at</a></strong><br /><br /><strong>ELIGIBILITY</strong><br /><br />Candidates should be in their penultimate year of studies and graduating from an undergraduate or a master&rsquo;s degree program in 2025. We welcome applications from candidates studying any degree subject.</p><p>Please note local language fluency is required to work at our Continental Europe locations. When applying, you&rsquo;ll have to answer one virtual cover letter question in the local language. However, only English is required for Budapest or for a position within Equity Private Markets in Zurich.<br /><br /><strong>APPLICATION DEADLINES</strong></p><ul><li>Investments business areas: <strong>22 September 2023</strong></li><li>All other business areas: <strong>27 October 2023</strong></li></ul><p>The recruitment process takes place throughout the autumn semester. In some businesses, certain teams may recruit through to early next year. Please note, we recruit on a rolling basis and encourage you to apply as soon as you&rsquo;re ready.<br /><br /><strong>OUR BUSINESS AREAS</strong></p><p>At BlackRock, you can have a career that&rsquo;s exciting, rewarding and full of possibilities.<br />BlackRock offers roles in the following business areas:<br /><br /><strong>Analytics &amp; Risk</strong><br />Develop and deliver tailored advice, risk management and investment services to governments, corporations and many of the world&rsquo;s largest investors.<br />Locations: London, Budapest, Milan</p><p><strong>Business Management &amp; Strategy</strong><br />Drive key initiatives, collaborate with internal and external partners, and ensure coordinated delivery of our capabilities.<br />Locations: London, Budapest, Milan, Munich, and other European locations</p><p><strong>Business Operations</strong><br />Works with teams across the organization to deliver a seamless investment management process.<br />Locations: London, Edinburgh</p><p><strong>Finance &amp; Internal Audit</strong><br />Provide thought leadership across the firm&rsquo;s control and risk management environment to improve decision making and help achieve the firm&rsquo;s objectives.<br />Locations: London, Edinburgh, Budapest</p><p><strong>Human Resources</strong><br />Our work focuses on attracting and developing a diverse workforce. We strive to make BlackRock an inclusive place to work where every employee feels like they belong and can meaningfully contribute to advancing our purpose.<br />Locations: London</p><p><strong>Investments</strong><br />Build and analyze investment strategies, portfolios and products for a broad range of institutional and retail clients.<br />Locations: London, Budapest, Copenhagen, Paris, Zurich, and other European locations</p><p><strong>Legal &amp; Compliance</strong><br />Understand and advise on the impact of regulatory requirements and best practices, protect the firm&rsquo;s reputation and partner with the business to meet our clients&rsquo; expectations.<br />Locations: London, Budapest</p><p><strong>Marketing &amp; Communications</strong><br />Develop strategies that grow our brands and company purpose to drive profit, build relationships with both existing and new clients, and solve systemic client needs in key areas across the firm.<br />Locations: London</p><p><strong>Sales &amp; Relationship Management</strong><br />Develop and deliver investment products and solutions for clients worldwide. Create marketing strategies around these solutions and use them to build new client relationships.<br />Locations: London, Edinburgh, Budapest, Paris, Madrid, Milan, Munich, Zurich and other European locations</p><p><strong>Technology</strong><br />Build and support Aladdin, the technology platform that powers our firm and the investment industry as a whole. Utilize sophisticated technology to develop applications, deliver custom solutions and deliver a seamless end to end investment management process.<br />Locations: London, Edinburgh, Budapest, Belgrade, Milan<br /><br /><strong>APPLICATION INSTRUCTIONS</strong></p><p>As part of your application, you will be able to select up to two teams you would like to be considered for. Read more about our business areas and detailed job descriptions <a href=\"https://careers.blackrock.com/early-careers/europe/#tab-id-4\"><strong>here</strong>.</a></p><p>Once you&rsquo;ve submitted your online application, you will be asked to complete a virtual cover letter. Think of this as an opportunity to give us your elevator pitch &ndash; it is not an interview but will be considered along with your resume. If you are interested in our software engineering opportunities, we&rsquo;ll ask you to complete a coding challenge instead of a virtual cover letter.</p><p>The virtual cover letter and/or coding challenge must be completed within 5 calendar days of submitting your initial application, for each business area you apply for.</p><p><a href=\"https://blackrock.tal.net/vx/brand-0/candidate/so/pm/1/pl/1/opp/7881-Summer-Internship-Program-EMEA/en-GB\"><strong>Apply here</strong></a></p>",
    "law": 1,
    "buisness": 1,
    "natural": 1,
    "it": 1,
    "education": 1,
    "other": 1,
    "arts": 1,
    "social": 1,
    "engineering": 1,
    "communication": 1,
    "medical": 1,
    "org_id": 835,
    "org_name": "BlackRock",
    "zip_code": "0000",
    "city": "and other European locations",
    "contact_person": {
      "salutation": "mrs",
      "title": "",
      "first_name": "Franziska",
      "last_name": "Hirschhäuser",
      "position": "personal",
      "position_spec": "",
      "mail": "franziska.hirschhaeuser@grips-design.de",
      "phone": "+496441500140"
    },
    "further_departments": [
      {
        "zip_code": "UK EH3 8BL",
        "city": "Edinburgh",
        "country": "other"
      },
      {
        "zip_code": "47 Vaci Ut",
        "city": "Budapest",
        "country": "other"
      }
    ],
    "links": [
      {
        "link": "https://careers.blackrock.com/early-careers/europe/#tab-id-4",
        "link_text": "BlackRock Careers"
      },
      {
        "link": "https://blackrock.tal.net/vx/brand-0/candidate/so/pm/1/pl/1/opp/7881-Summer-Internship-Program-EMEA/en-GB",
        "link_text": "Apply here"
      }
    ],
    "files": [
      {
        "file": "20230825_Stellenanzeige_Werkstudent_IT.pdf",
        "file_description": "Ausschreibung als PDF",
        "file_link": "TODO"
      }
    ]
  }
}
```
