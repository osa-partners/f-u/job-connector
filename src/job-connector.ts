/*
 * Copyright (C) 2020-2022 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ConnectorClient} from '@openstapps/api';
import {
  SCJobCategories,
  SCJobPosting,
  SCLicensePlate,
  SCOrganizationWithoutReferences,
  SCThingOriginType,
  SCThingRemoteOrigin,
  SCThingType,
} from '@openstapps/core';
import {Logger} from '@openstapps/logger';
import {
  JobAPIResponse,
  JobDetails,
  JobPreview,
  OrgAPIResponse,
  OrgDetails,
  OrgPreview,
  PaginatedAPIResponse,
} from './types.js';
import {request} from 'undici';

const baseURL = 'https://api.stellenportal-uni-frankfurt.de/';

/**
 * Creates a remote origin
 * @param name The name of the origin
 * @param jobId The id of the job
 */
export function createOriginJobDetails(name: string, jobId: number): SCThingRemoteOrigin {
  return {
    indexed: new Date().toISOString(),
    name,
    type: SCThingOriginType.Remote,
    url: `${baseURL}job?id=${jobId}`,
  };
}

/**
 * extracts the categories out of the jobDetails and returns them as an array
 * @param jobDetails API result of a sinle job
 */
function getJobCategories(jobDetails: JobDetails) {
  const categories: SCJobCategories[] = [];
  for (const category of [
    'law',
    'business',
    'natural',
    'it',
    'education',
    'other',
    'arts',
    'social',
    'engineering',
    'communication',
    'medical',
  ] as const) {
    if (jobDetails[category]) categories.push(category);
  }
  return categories;
}

/**
 * Fetch all items from a paginated endpoint
 */
async function fetchPaginated<const Endpoint extends string, T>(endpoint: Endpoint): Promise<T[]> {
  let nextPageUrl = `${baseURL}${endpoint}`;
  const results: T[] = [];
  while (nextPageUrl) {
    //Logger.info('Fetching', nextPageUrl);
    const jobsResponse = await request(nextPageUrl);
    if (jobsResponse.statusCode !== 200) {
      throw new Error(`Failed to fetch data from ${nextPageUrl}`);
    }
    const responseData: PaginatedAPIResponse<Endpoint, T> =
      (await jobsResponse.body.json()) as PaginatedAPIResponse<Endpoint, T>;
    const data = responseData[endpoint].data;
    for (const entry of data) {
      results.push(entry);
    }
    nextPageUrl = responseData[endpoint].next_page_url;
  }
  return results;
}

/**
 * fetch job details by ID
 */
async function fetchJobDetails(jobId: number): Promise<JobAPIResponse> {
  try {
    const apiUrl = `${baseURL}job?id=${jobId}`;
    const jobResponse = await request(apiUrl);
    if (jobResponse.statusCode !== 200) {
      throw new Error(`Failed to fetch job details for ID ${jobId}`);
    }
    return jobResponse.body.json() as unknown as JobAPIResponse;
  } catch (error) {
    Logger.error(`Error fetching job details for ID ${jobId}:`, error);
    throw error;
  }
}

/**
 * fetch organization details by ID
 */
async function fetchOrgDetails(orgId: number): Promise<OrgAPIResponse> {
  try {
    const apiUrl = `${baseURL}org?id=${orgId}`;
    const orgResponse = await request(apiUrl);
    if (orgResponse.statusCode !== 200) {
      throw new Error(`Failed to fetch org details for ID ${orgId}`);
    }
    return orgResponse.body.json() as unknown as OrgAPIResponse;
  } catch (error) {
    Logger.error(`Error fetching org details for ID ${orgId}:`, error);
    throw error;
  }
}

// function assignToInterface(tsInterface: any, source: any) {
//   for (const [key, value] of Object.entries(source)) {
//     if (!tsInterface[key]) tsInterface[key] = [];
//     if (value === null) {
//       if (!tsInterface[key].includes(null)) tsInterface[key].push(null);
//       continue;
//     }
//     const type = typeof value;
//     if (type === 'object') {
//       let existingObject = tsInterface[key].find((entry: any) => !!entry && typeof entry === 'object');
//       if (!existingObject) {
//         existingObject = {};
//         tsInterface[key].push(existingObject);
//       }
//       assignToInterface(existingObject, value);
//       continue;
//     }
//     if (!tsInterface[key].includes(type)) tsInterface[key].push(type);
//   }
// }

/**
 * Fetches the jobs from the job portal of the goethe university
 */
export async function fetchJobs(licensePlate: SCLicensePlate, originName: string): Promise<SCJobPosting[]> {
  try {
    const scJobPostingPreviews = await fetchPaginated<'jobs', JobPreview>('jobs');
    const scOrgPreviews = await fetchPaginated<'orgs', OrgPreview>('orgs');

    Logger.info('Fetching all jobs...');
    const jobDetailsResponses: JobAPIResponse[] = [];
    for (const job of scJobPostingPreviews) {
      //Logger.info('Fetching job details for job', job.job_id);
      jobDetailsResponses.push(await fetchJobDetails(job.job_id));
    }

    Logger.info('Fetching all orgs...');
    const orgDetailResponses: OrgAPIResponse[] = [];
    for (const org of scOrgPreviews) {
      //Logger.info('Fetching org details for org', org.portrait_id);
      orgDetailResponses.push(await fetchOrgDetails(org.portrait_id));
    }

    const scOrgDetails = orgDetailResponses.map(response => response.org).filter(Boolean) as OrgDetails[];

    const scJobDetails = jobDetailsResponses.map(response => response.job).filter(Boolean) as JobDetails[];

    const scJobPostings: SCJobPosting[] = scJobDetails.map(jobDetails => {
      const orgDetails = scOrgDetails.find(org => org.org_id === jobDetails.org_id);
      const org: SCOrganizationWithoutReferences = orgDetails
        ? {
            type: SCThingType.Organization,
            name: orgDetails.name,
            uid: ConnectorClient.makeUUID(orgDetails.org_id.toString(), licensePlate),
            identifiers: {org_id: orgDetails.org_id.toString()},
            image: `${baseURL}${orgDetails.logo_path}`,
            description: orgDetails.description,
            sameAs: `https://stellenportal-uni-frankfurt.de/firmentreffer/?id=${orgDetails.org_id}`,
          }
        : {
            type: SCThingType.Organization,
            name: jobDetails.org_name,
            uid: ConnectorClient.makeUUID(jobDetails.org_id.toString(), licensePlate),
            image: jobDetails.logo_path ? `${baseURL}${jobDetails.logo_path}` : undefined,
            identifiers: {org_id: jobDetails.org_id.toString()},
          };
      return {
        identifiers: {job_id: jobDetails.job_id.toString()},
        name: jobDetails.job_name,
        employerOverview: org,
        image: jobDetails.logo_path ? `${baseURL}${jobDetails.logo_path}` : undefined,
        inPlace: {
          type: SCThingType.PointOfInterest,
          uid: ConnectorClient.makeUUID(jobDetails.job_id.toString(), licensePlate),
          name: `Location of job with id ${jobDetails.job_id}`,
          geo: {point: {type: 'Point', coordinates: [0, 0]}},
          categories: [],
          address: {
            addressCountry: 'de',
            addressLocality: jobDetails.city,
            postalCode: jobDetails.zip_code,
            streetAddress: '',
          },
        },
        sameAs: `https://stellenportal-uni-frankfurt.de/jobdetails/?job=${jobDetails.job_id}`,
        description: jobDetails.description,
        type: SCThingType.JobPosting,
        uid: ConnectorClient.makeUUID(jobDetails.job_id.toString(), licensePlate),
        origin: createOriginJobDetails(originName, jobDetails.job_id),
        categories: getJobCategories(jobDetails),
      } satisfies SCJobPosting;
    });

    return scJobPostings;
  } catch (error) {
    await Logger.error(error);
    throw error;
  }
}
