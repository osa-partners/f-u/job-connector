import {ConnectorClient, HttpClient} from '@openstapps/api';
import {Logger} from '@openstapps/logger';
import {Command, Option} from 'commander';
import {version} from '../package.json';
import {fetchJobs} from './job-connector.js';

const commander = new Command()
  .version(version)
  .addOption(
    new Option('-b, --backend <string>', 'The StApps backend url')
      .default('http://localhost:3000')
      .env('STAPPS_BACKEND'),
  )
  .parse(process.argv);

process.on('unhandledRejection', async error => {
  await Logger.error('UNHANDLED REJECTION', error);
  process.exit(1);
});

interface Options {
  backend: string;
}

const {backend} = commander.opts<Options>();

try {
  const jobs = await fetchJobs('f-u', 'Goethe-Uni Stellenportal');
  const client = new ConnectorClient(new HttpClient(), backend);
  await client.index(
    jobs.filter(
      job =>
        !(job.description?.length && job.description?.length > 20_000) &&
        !(job.employerOverview?.description?.length && job.employerOverview?.description?.length > 20_000),
    ),
    'job-connector',
  );
  Logger.ok('Done');
} catch (error: unknown) {
  await Logger.error('Caught Error: ' + error);
}
