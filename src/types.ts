export type APIResponse<Endpoint extends string, Data> = {
  [T in Endpoint]: Data;
};

export type PaginatedAPIResponse<Endpoint extends string, Data> = APIResponse<
  Endpoint,
  {
    current_page: number;
    data: Data[];
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    links: Array<{url?: string | null; label: string; active: boolean}>;
    next_page_url: string;
    path: string;
    per_page: number;
    prev_page_url: string;
    to: number;
    total: number;
  }
>;

export type OrgAPIResponse = APIResponse<'org', OrgDetails | null>;

export interface OrgPreview {
  portrait_id: number;
  org_id: number;
  name: string;
  org_logo: string;
  logo_path: string;
}

export interface OrgDetails {
  org_id: number;
  portrait_id: number;
  name: string;
  description: string;
  url?: string | null;
  url_broschure?: string | null;
  law: number;
  business: number;
  natural: number;
  it: number;
  education: number;
  other: number;
  arts: number;
  social: number;
  engineering: number;
  communication: number;
  medical: number;
  org_logo: string;
  logo_path: string;
  departments: OrgDepartment[];
  links: Link[];
  files: File[];
  videos: Video[];
}

export interface OrgDepartment {
  id: number;
  head_office: number;
  zip_code: string;
  city: string;
  street?: string | null;
  country: string;
  country_spec?: null | string;
  phone?: null | string;
  mail?: null | string;
}

export type JobAPIResponse = APIResponse<'job', JobDetails | null>;

export interface JobPreview {
  job_id: number;
  job_name: string;
  org_id: number;
  org_name: string;
  org_logo: string;
  zip_code: string;
  city: string;
  logo_path: string;
}

export interface JobDetails {
  job_id: number;
  job_name: string;
  description: string;
  law: boolean;
  business: boolean;
  natural: boolean;
  it: boolean;
  education: boolean;
  other: boolean;
  arts: boolean;
  social: boolean;
  engineering: boolean;
  communication: boolean;
  medical: boolean;
  org_id: number;
  org_name: string;
  org_logo?: string | null;
  person_id?: number | null;
  zip_code: string;
  city: string;
  logo_path?: string | null;
  contact_person?: JobContactPerson | null;
  further_departments: JobDepartment[];
  links: Link[];
  files: File[];
}

export interface JobDepartment {
  zip_code: string;
  city: string;
  country?: string | null;
}

export interface JobContactPerson {
  salutation: string;
  title: string;
  first_name: string;
  last_name: string;
  position: string;
  position_spec: string;
  mail: string;
  phone: string;
}

export interface Link {
  link: string;
  link_text: string;
}

export interface Video {
  video_link: string;
  video_text?: string | null;
}

export interface File {
  file: string;
  file_description: string;
  file_link: string;
}
