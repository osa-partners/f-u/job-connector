FROM registry.gitlab.com/openstapps/openstapps/node-builder as pruned

USER root

COPY . /app
WORKDIR /app

RUN pnpm config set store-dir .pnpm-store
RUN pnpm --filter=job-connector --prod deploy pruned


FROM registry.gitlab.com/openstapps/openstapps/node-base

USER node

COPY --from=pruned --chown=node /app/pruned /app
WORKDIR /app

CMD ["node", "app.js"]
