# Job Connector

## Installation

```bash
pnpm install
pnpm run build
```

## Usage

```bash
node lib/cli.js -b $STAPPS_BACKEND
```

## Structure

We're pulling all jobs from the official API of Goethe University. Then we scrape all their detials and the organization's details. Find more information about the API we're using ("Stellenportal"-API) in `STELLENPORTAL-API.md`.

## Tests

```bash
pnpm test
```

### Polling the data source

Start the programm / docker image multiple times per day, depending on the update fequency of the data.

### Docker Image

The Gitlab CI / CD publishes Docker images if a tag (version) from the master branch is generated.
Value for `$STAPPS_BACKEND` has to be provided in Gitlab CI / CD Settings of the repo.

The lastest Docker image is available at: **registry.gitlab.com/openstapps/f-u/job-connector:latest** .
For other versions see available tags in the repo and change the tag at the end of the docker registry address if needed.

**Usage:**
If you run the image don't forget to set STAPPS_BACKEND to point the full URL of the backend.

```bash
docker run -e STAPPS_BACKEND='http://backend-host:port' registry.gitlab.com/openstapps/f-u/job-connector:latest
```
